<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_masuk</name>
   <tag></tag>
   <elementGuidId>cc2018b8-638a-4e24-ae6f-72719d08569a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-primary w-100']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-primary w-100']</value>
      <webElementGuid>de518c4b-da94-4ed3-a5c7-3ec1a3a698cb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
